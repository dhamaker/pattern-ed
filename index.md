---
title: Contents
layout: page
---
{% assign posts=site.posts | sort : "title" %}
{% for page in posts %}
  <article class="card">
    <h2><a href="{{site.baseurl}}{{page.url}}">{{page.title}}</a></h2>
    <p class="description">{{page.description}}</p>
    <p class="published">{{page.date | date: "%B %d, %Y" }}, {{page.author}}</p>
  </article>
{% endfor %}
