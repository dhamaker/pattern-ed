---
title: Tags
layout: page
---
{% assign tags = site.tags | sort %}
{% for item in tags  %}
  <h2>{{ item[0] | capitalize }}</h2>
  <ul>
    {% assign items = item[1] | sort : "title" %}
    {% for post in items %}
      <li><a href="{{site.baseurl}}{{ post.url }}">{{ post.title }}</a></li>
    {% endfor %}
  </ul>
{% endfor %}
