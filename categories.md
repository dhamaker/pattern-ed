---
title: Categories
layout: page
---
{% assign categories=site.categories | sort%}
{% for item in categories %}
  <h2>{{ item[0] | capitalize  }}</h2>
  <ul>
    {% assign items = item[1] | sort : "title" %}
    {% for post in items %}
      <li><a href="{{site.baseurl}}{{ post.url }}">{{ post.title }}</a></li>
    {% endfor %}
  </ul>
{% endfor %}
