---
title: Toggletip
status: draft
category: custom
tags: button, tooltip
a11y-audit:  {none; [standard] + (pass, fail [#issues])}
a11y-report: {none, [url]}
shield-status: {none, backlog, [url]}
---

# Toggletip

This is a _custom_ component. Each product team is responsible for understanding and using the `aria-*` attributes to ensure the pattern is accessible.  

You may use a toggletip to provide non-essential, supplemental information about a subject.  Toggletips should be used sparingly and designers should always consider more simple alternatives, including editorial guidelines and other visible methods, before adding a toogletip. Using tooltips to reduce visual clutter can increase verbosity for screenreaders user.

A toggletip is always used to modify another object or collection - an article, section, heading, input component, widget, etc.  It consists of a toggle, or button, and a tip, or phrase, which remains hidden until request by the user.  

## Shield 

*  The Shield [toggletip](https://shield.us.bank-dns.com/usbank/main/latest/components/Tooltip) uses the following aria attributes:
    * `aria-label` 
    * `aria-expanded`
    * `aria-live` 
* The toggle is a button with `aria-label` and `aria-expanded` attributes.
* The tip container is an empty `aria-live` region with using a `polite` voice.
* When toggletip is opened, 
    * Button `aria-expanded` is enabled,
    * The tip message is posted to the `aria-live` region where it is announced, and
    * The tip message is made visible.
* When toggletip is closed,
  * The button `aria-expanded` is returned to the original state.
  * The message is hidden, and
  * The `aria-live` region is cleared.

## Content

Authors have two objects to write.  These should be writen as part of their parent section to make sure they are relevant in the page context

* label:  the toggle label identifies the purpose and subject of the tooggletip. Label must be unique per document except if toggletip is  duplicate content.
* description: the tip copy.

## Design and development guidance

* Always code toggletips as buttons. 
* Button must have unique label copy (expcept when content is duplicated) so reader can correctly identify the purpose and subject.
* Tip must be positioning relative to button should be perceiveable to readers using screen magnifier.
* Toggle button must have sufficient contrast with surroundings.
* Tip container must have sufficient contrast with surroundings.
* Tip container must be responsive to changes in viewport size and orientation to avoid offscreen positioning and unintended scrolling.
* Tip container must reflow with changes in font size, text spacing, and line height to avoid ‘clipping’ content.

### Related patterns
| Category | Patterns |
|---|---|
| Shield subcomponents | [button](https://shield.us.bank-dns.com/usbank/main/latest/components/button]) and text patterns|
| General variants | tooltip |
| Comparitives |  menu, dialog, tool button, popover. |

## Tab and focus experienece

* A toggletip is controlled by explicit user action, a button activation. It is not controlled by on focus of on hover events.
* <kbd>Tab</kbd> key focuses the button.
* <kbd>Space</kbd> or <kbd>Enter</kbd> activates the toggle tip. 
* <kbd>esc</kbd> key dismisses toggle tips. 
* When a user activates a toggle tip, the screen reader announces the content without changing focus (see Issues section)
* When navigating forward, a screen reader user hears the toggle tip content announced again. NVDA screen reader users hear the toggle tip content for the first time. 

## Screen reader experience

### Read Order

__On focus__

The button announces "What is [subject]" and the expanded state of the button.

__On open__ 

The button announces the same label, the expanded state of the button, and the tip content as live polite message.  Focus does not change.

__(optional) Read forward__  

The screen reader moves forward to the next text element, the tip message, and the Tip is announced again.

__On close__ 

The button again announces the label and the expanded state of the button.  Focus does not change.

### Announcement example

> tbd

## Issues
* NVDA screen reader does not automatically announce tip on button activation.
* Beware of too many toggletips in one experience.  This increase the noise and distractions for screenreader. 
* No message on tabbing to expanded button.  If users opens toggletip, tabs to the next control, then moves back to the toggletip, the label and the expanded state will be announced.  The description will not announce.
* Does __not__ use Deque's `aria-describedby` and `role=tooltip` method for [Tooltip Dialog](https://dequeuniversity.com/class/custom-widgets2/examples/tooltip-dialog).


## Alternate solutions
> Document any alternatives implmentations researched.

* Do not use `aria-describedby` on toggletip buttons.  This is a tooltip technique that you may see  used with toggletip buttons.  It is redundant and a bad experience.
* Eval variations of img with alt and title
* Eval variations of 'a img' with alt, title and links to glossary or other contetn'


## Related Success Criteria

## Reference

- [Tooltip Dialog | Deque University] (https://dequeuniversity.com/class/custom-widgets2/examples/tooltip-dialog). Unused dialog method.

- [Tooltips &amp; Toggletips | Inclusive Components](https://inclusive-components.design/tooltips-toggletips/), Heydon Pickering.  Good discusion of alternate 'aria-live' experiences.
